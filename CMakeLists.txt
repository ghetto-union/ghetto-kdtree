project (libkdtree CXX)
cmake_minimum_required (VERSION 2.6.0)

option (libkdtree_build_python_bindings "Build Python bindings (requires SWIG)." OFF)
option (libkdtree_build_examples "Build libkdtree examples." ON)

if (WIN32)

   # Maximum warning level
   set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")

   # Be strict about warnings... make them errors
   set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /WX")

   # Detect 64-bit portability issues
   set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Wp64")

else (WIN32)

   # Maximum warning level
   set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

   # turn on debugging
   set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")

endif (WIN32)

if (libkdtree_build_python_bindings)
   add_subdirectory (python-bindings)
endif ()

include_directories (${PROJECT_SOURCE_DIR})

if (libkdtree_build_examples)
  add_subdirectory(examples)
endif()

file (GLOB KDTREE_HEADERS kdtree++/*.hpp)
add_library(libkdtree INTERFACE)
target_sources(libkdtree INTERFACE ${KDTREE_HEADERS})
if (DEFINED CMAKE_VERSION AND NOT "${CMAKE_VERSION}" VERSION_LESS "2.8.11")
  target_include_directories(libkdtree INTERFACE "${libkdtree_SOURCE_DIR}")
endif()

install (FILES ${KDTREE_HEADERS} DESTINATION ${CMAKE_INSTALL_PREFIX}/include)
